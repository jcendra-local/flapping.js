(in-package :flapping)

(defparameter *bird-max-speed* 5)
(defparameter *bird-min-speed* 1.3)

(defclass bird (map-object)
  ((owner :initform nil
          :accessor owner)
   (owner-cell :initform nil
               :accessor owner-cell)
   (new-dx :initform 0 :type fixnum :accessor new-dx)

   ;; debug values
   (sum-pos-x :initform 0 :type fixnum :accessor sum-pos-x)
   (sum-pos-y :initform 0 :type fixnum :accessor sum-pos-y)
   (sum-pos-weights :initform 0 :type fixnum :accessor sum-pos-weights)
   (sum-dir-x :initform 0 :type fixnum :accessor sum-dir-x)
   (sum-dir-y :initform 0 :type fixnum :accessor sum-dir-y)
   (sum-dir-weights :initform 0 :type fixnum :accessor sum-dir-weights)
   (sum-sep-x :initform 0 :type fixnum :accessor sum-sep-x)
   (sum-sep-y :initform 0 :type fixnum :accessor sum-sep-y)
   (sum-sep-weights :initform 0 :type fixnum :accessor sum-sep-weights)
   (force-x :initform 0 :type fixnum :accessor force-x)
   (force-y :initform 0 :type fixnum :accessor force-y)
   (angle-middle :initform 0 :type fixnum :accessor angle-middle)))

(defmethod initialize-instance :after ((bird bird) &key)
  (setf (owner-cell bird)
        (dll-make-cell bird)))

(defgeneric sqr-distance-from-object (map-object-1 map-object-2)
    (:method ((o1 map-object) (o2 map-object))
        (+ (expt (- (x o1) (x o2)) 2)
            (expt (- (y o1) (y o2)) 2))))


(defgeneric update-object (obj grid)
  (:method ((bird bird) (grid grid))
    (with-accessors ((x x) (y y)
                     (dx dx) (dy dy)
                     (new-dx new-dx)
                     (new-dy new-dy)) bird

      (let* ((new-x (limit-coord (+ x new-dx) grid))
             (new-y (limit-coord (+ y new-dy) grid))
             (old-tile (tile bird))
             (new-tile (tile-from-coord grid new-x new-y)))
            (with-lock-held ((write-lock bird))
              (setf x new-x)
              (setf y new-y)
              (setf dx new-dx)
              (setf dy new-dy))

            (if (eq new-tile old-tile) nil bird)))))

(defgeneric tick (obj grid)
  (:method ((bird bird) (grid grid))
    (calculate-flock grid bird 150 75 3 1/200 1/2 nil)))

;;; Divide, except x/0 == 0
(defun /0 (divident divisor)
       (if (/= divisor 0)
           (/ divident divisor)
           0))

(defun angle-diff (x1 y1 x2 y2)
  (- (mod (+  (- (atan y2 x2) (atan (- y1) (- x1)))
              (* 2 pi))
          (* 2 pi))
     pi))

(defun pythagoras (x y)
  (sqrt (+ (expt x 2)
           (expt y 2))))

(defun limit (x min max)
  (min (max min x) max))

(defun calculate-near-bird-sums (grid bird scan-distance sep-distance)
  (let ((sum-pos-x 0) (sum-pos-y 0) (sum-pos-weights 0)
        (sum-dir-x 0) (sum-dir-y 0) (sum-dir-weights 0)
        (sum-sep-x 0) (sum-sep-y 0) (sum-sep-weights 0))
    (with-accessors ((x x) (y y)
                     (dx dx) (dy dy)
                     (new-dx new-dx) (new-dy new-dy)) bird
      (loop-objects-within-distance grid near-bird x y scan-distance
           (unless (equalp bird near-bird)
             (with-accessors ((n-x x) (n-y y)
                              (n-dx dx) (n-dy dy)) near-bird

               (let* ((distance (sqrt (sqr-distance-from-object near-bird bird)))
                      (dist-fraction (/ distance scan-distance))
                      (attr-weight (+ 11/10 (/ 1 
                                               (- (* 10/11 10 dist-fraction) 10))))) ; broken, returns between 0.99 and 1

                 ;; Attraction
                 (incf sum-pos-x (* (- n-x x) attr-weight))
                 (incf sum-pos-y (* (- n-y y) attr-weight))
                 (incf sum-pos-weights attr-weight)

                 ;; Unidirection
                 (incf sum-dir-x (* n-dx dist-fraction))
                 (incf sum-dir-y (* n-dy dist-fraction))
                 (incf sum-dir-weights dist-fraction)


                 ;; Separation
                 (when (< distance sep-distance)
                   (if (= distance 0) ;If birds overlap, wiggle them a little
                       (progn  (incf sum-sep-x (* (/ (- (random 1000) 500) 1000) sep-distance))
                               (incf sum-sep-y (* (/ (- (random 1000) 500) 1000) sep-distance)))
                       (progn (let ((sep-weight (/ sep-distance distance)))
                                (incf sum-sep-x (* (/ (- x n-x)
                                                      distance)
                                                   sep-weight))
                                (incf sum-sep-y (* (/ (- y n-y)
                                                      distance)
                                                   sep-weight))
                                (incf sum-sep-weights sep-weight))))))))))
    (list (list sum-pos-x sum-pos-y sum-pos-weights)
          (list sum-dir-x sum-dir-y sum-dir-weights)
          (list sum-sep-x sum-sep-y sum-sep-weights))))


(defun calculate-new-d-pos (near-bird-sums bird scale-pos scale-dir)
  (with-accessors ((x x) (y y)
                   (dx dx) (dy dy)
                   (new-dx new-dx) (new-dy new-dy)) bird
    (destructuring-bind ((sum-pos-x sum-pos-y sum-pos-weights)
                         (sum-dir-x sum-dir-y sum-dir-weights)
                         (sum-sep-x sum-sep-y sum-sep-weights)) near-bird-sums
      (let* ((force-x (+ (* sum-sep-x)
                         (* scale-pos (/0 sum-pos-x sum-pos-weights))
                         (* scale-dir (/0 sum-dir-x sum-dir-weights))))
             (force-y (+ (* sum-sep-y)
                         (* scale-pos (/0 sum-pos-y sum-pos-weights))
                         (* scale-dir (/0 sum-dir-y sum-dir-weights))))
             (angle-middle (angle-diff dx dy force-x force-y)))

        ;; (setf angle-middle 1/20)
        (setf (force-x bird) force-x)
        (setf (force-y bird) force-y)
        (setf (angle-middle bird) angle-middle)
        

        ;; (setf angle-middle -0.1)
        (log-message :debug-flocking "Force: ~s,~s; angle-middle: ~s"
                     force-x force-y
                     angle-middle)
        
        ;; Turn
        (let* ((curr-speed (pythagoras dx dy))
               (turn-degree (* angle-middle
                               (/ (- (+ *bird-max-speed* *bird-min-speed*)
                                     curr-speed)
                                  *bird-max-speed*)
                               2/5))

               (turn-x (- (* dx (cos turn-degree)) (* dy (sin turn-degree))))
               (turn-y (+ (* dx (sin turn-degree)) (* dy (cos turn-degree)))))

          (log-message :debug-flocking "curr-speed: ~s; turn-degree: ~s; turn: ~s,~s"
                       curr-speed
                       turn-degree
                       turn-x turn-y)
          
          ;; Accel
          (let* ((turn-slowing (cos angle-middle))
                 (force-size (sqrt (+ (expt force-x 2)
                                      (expt force-y 2))))
                 (accel (* force-size
                           (- turn-slowing 0.5)
                           12/100))

                 (speed (pythagoras turn-x turn-y)))

            (log-message :debug-flocking "turn-slowing: ~s; force-size: ~s; accel: ~s; speed: ~s"
                         turn-slowing force-size accel speed)

            (let* ((new-speed (limit (- (+ speed accel)
                                        (* (expt (+ speed accel) 2 ) ;penalty to bleed of speed
                                           2/1000))
                                     *bird-min-speed*
                                     *bird-max-speed*)))

              (log-message :debug-flocking "new-speed: ~s"
                           new-speed)

              (log-message :debug-flocking "new-d: ~s, ~s;"
                           new-dx new-dy)


              ;; (setf new-speed *bird-min-speed*)
              (let ((turn-angle (atan turn-y turn-x)))
                (list (* new-speed (cos turn-angle))
                      (* new-speed (sin turn-angle)))))))))))


(defun set-new-d-pos (grid bird new-dx new-dy)
  ;; (setf (new-dx bird) new-dx)
  ;; (setf (new-dy bird) new-dy)


  (setf (new-dx bird)
        (- (limit-coord (+ (x bird) new-dx) grid)
           (x bird)))

  (setf (new-dy bird)
        (- (limit-coord (+ (y bird) new-dy) grid)
           (y bird))))

(defun new-calculate-flock (grid bird scan-distance sep-distance scale-pos scale-dir)
  (let* ((sums (calculate-near-bird-sums grid bird scan-distance sep-distance)))
    (destructuring-bind (new-d-pos-x new-d-pos-y) (calculate-new-d-pos sums bird scale-pos scale-dir)
      (set-debug-sums bird sums)

      
      (set-new-d-pos grid bird
                     new-d-pos-x
                     new-d-pos-y))))

(defun set-debug-sums (bird sums)
  (destructuring-bind ((sum-pos-x sum-pos-y sum-pos-weights)
                       (sum-dir-x sum-dir-y sum-dir-weights)
                       (sum-sep-x sum-sep-y sum-sep-weights)) sums

    (setf (sum-pos-x bird) sum-pos-x)
    (setf (sum-pos-y bird) sum-pos-y)
    (setf (sum-pos-weights bird) sum-pos-weights)
    (setf (sum-dir-x bird) (* 30 sum-dir-x))
    (setf (sum-dir-y bird) (* 30 sum-dir-y))
    (setf (sum-dir-weights bird) sum-dir-weights)
    (setf (sum-sep-x bird) sum-sep-x)
    (setf (sum-sep-y bird) sum-sep-y)
    (setf (sum-sep-weights bird) sum-sep-weights)))

(defgeneric calculate-flock (grid bird scan-distance sep-distance scale-dir scale-pos scale-sep user)
  (:method ((grid grid) (bird bird) scan-distance sep-distance scale-dir scale-pos scale-sep user)
    (declare (ignorable user))

    (new-calculate-flock grid bird scan-distance sep-distance scale-pos scale-dir)

    ;; (log-message :debug-flocking "Starting calculate-flock for bird")
    ;; (let* ((sum-pos-x 0) (sum-pos-y 0) (sum-pos-weights 0)
    ;;        (sum-dir-x 0) (sum-dir-y 0) (sum-dir-weights 0)
    ;;        (sum-sep-x 0) (sum-sep-y 0) (sum-sep-weights 0))

    ;;   (with-accessors ((x x) (y y)
    ;;                    (dx dx) (dy dy)
    ;;                    (new-dx new-dx) (new-dy new-dy)) bird
    ;;     (loop-objects-within-distance grid near-bird x y scan-distance
    ;;          (unless (equalp bird near-bird)
    ;;            (with-accessors ((n-x x) (n-y y)
    ;;                             (n-dx dx) (n-dy dy)) near-bird

    ;;              (let* ((distance (sqrt (sqr-distance-from-object near-bird bird)))
    ;;                     (dist-fraction (/ distance scan-distance))
    ;;                     (attr-weight (+ 1.1 (/ 1 
    ;;                                            (- (* 10/11 dist-fraction) 10)))))

    ;;                ;; Attraction
    ;;                (incf sum-pos-x (* (- n-x x) attr-weight))
    ;;                (incf sum-pos-y (* (- n-y y) attr-weight))
    ;;                (incf sum-pos-weights attr-weight)

    ;;                ;; Unidirection
    ;;                (incf sum-dir-x (* n-x dist-fraction))
    ;;                (incf sum-dir-y (* n-y dist-fraction))
    ;;                (incf sum-dir-weights dist-fraction)

    ;;                ;; Separation
    ;;                (when (< distance sep-distance)
    ;;                  (if (= distance 0) ;If birds overlap, wiggle them a little
    ;;                      (progn  (incf sum-sep-x (* (/ (- (random 1000) 500) 1000) sep-distance))
    ;;                              (incf sum-sep-y (* (/ (- (random 1000) 500) 1000) sep-distance)))
    ;;                      (progn (let ((sep-weight (/ sep-distance distance)))
    ;;                               (incf sum-sep-x (* (/ (- x n-x)
    ;;                                                     distance)
    ;;                                                  sep-weight))
    ;;                               (incf sum-sep-y (* (/ (- y n-y)
    ;;                                                     distance)
    ;;                                                  sep-weight))
    ;;                               (incf sum-sep-weights sep-weight)))))))))


        
        
    ;;     (log-message :debug-flocking "sum-pos: ~s,~s; sum-pos-weights: ~s; sum-dir: ~s,~s; sum-dir-weights: ~s; sum-sep: ~s,~s; sum-sep-weights: ~s"
    ;;                  sum-pos-x sum-pos-y
    ;;                  sum-pos-weights
    ;;                  sum-dir-x sum-dir-y
    ;;                  sum-pos-weights
    ;;                  sum-sep-x sum-sep-y
    ;;                  sum-sep-weights)

    ;;     (let* ((force-x (+ sum-sep-x
    ;;                        (* scale-pos (/0 sum-pos-x sum-pos-weights))
    ;;                        (* scale-dir (/0 sum-dir-x sum-dir-weights))))
    ;;            (force-y (+ sum-sep-y
    ;;                        (* scale-pos (/0 sum-pos-y sum-pos-weights))
    ;;                        (* scale-dir (/0 sum-dir-y sum-dir-weights))))
    ;;            (angle-middle (angle-diff dx dy force-x force-y)))

    ;;       (log-message :debug-flocking "Force: ~s,~s; angle-middle: ~s"
    ;;                    force-x force-y
    ;;                    angle-middle)

    ;;       ;; Turn
    ;;       (let* ((curr-speed (pythagoras dx dy))
    ;;              (turn-degree (* angle-middle (- (+ *bird-max-speed* *bird-min-speed*)
    ;;                                              curr-speed)
    ;;                              8/100))

    ;;              (turn-x (- (* dx (cos turn-degree)) (* dy (sin turn-degree))))
    ;;              (turn-y (+ (* dx (sin turn-degree)) (* dy (cos turn-degree)))))

    ;;         (log-message :debug-flocking "curr-speed: ~s; turn-degree: ~s; turn: ~s,~s"
    ;;                      curr-speed
    ;;                      turn-degree
    ;;                      turn-x turn-y)
            
    ;;         ;; Accel
    ;;         (let* ((turn-slowing (cos angle-middle))
    ;;                (force-size (sqrt (+ (expt force-x 2)
    ;;                                     (expt force-y 2))))
    ;;                (accel (* force-size
    ;;                          (- turn-slowing 0.5)
    ;;                          12/100))

    ;;                (speed (pythagoras turn-x turn-y)))

    ;;           (log-message :debug-flocking "turn-slowing: ~s; force-size: ~s; accel: ~s; speed: ~s"
    ;;                        turn-slowing force-size accel speed)

    ;;           (let* ((new-speed (limit (- (+ speed accel)
    ;;                                       (* (expt (+ speed accel) 2 ) ;penalty to bleed of speed
    ;;                                          2/1000))
    ;;                                    *bird-min-speed*
    ;;                                    *bird-max-speed*)))

    ;;             (log-message :debug-flocking "new-speed: ~s"
    ;;                          new-speed)

    ;;             (let ((turn-angle (atan turn-y turn-x)))
    ;;               (setf new-dx (* new-speed (cos turn-angle)))
    ;;               (setf new-dy (* new-speed (sin turn-angle))))
                                                
    ;;             (log-message :debug-flocking "new-d: ~s, ~s;"
    ;;                          new-dx new-dy)))))))
    ))





