(in-package :flapping)

(defparameter *message-input* (make-queue))
(defparameter *message-output* (make-queue))

(defparameter *client* nil)
(defparameter *socket-path* "/tmp/node-sbcl.sock")

(defparameter *connection-thread* nil)

(defun run-listener ()
  (log-message :info "Starting listener...")
  (log-message :info "Launching listener task...")
  
  (setf *connection-thread*
    (bordeaux-threads:make-thread #'accept-connection-loop :name "Node socket connection reader-loop")))

(defun accept-connection-loop ()
  (loop
     (unwind-protect 
          (with-open-socket  (socket :address-family :local
                                     :type :stream
                                     :connect  :passive
                                     :local-filename *socket-path*
                                     :reuse-address t)
            (log-message :info "Waiting for client...")


            (setf *client* (accept-connection socket :wait t))
            (log-message :info "Accepted client's connection.")

            (log-message :info "Starting message sender")
            (start-msg-sender)
            (log-message :info "Saying hello...")

            (hello-node)

            (log-message :info "Starting message receiver")
            (handler-case (msg-receiver)
              (socket-connection-reset-error ()
                (log-message :node-error "Connection to node was reset by peer."))

              (end-of-file ()
                (log-message :node-error "Node closed connection for read."))

              (hangup ()
                (log-message :node-error "Node closed connection for a write.")))

            (log-message :info "Thread stopped for some reason. Looping and waiting for client connection again.")) 

       (stop-sender-loop)
       (delete-file *socket-path*))))


(defun stop-sender-loop ()
  (mapc #'(lambda (thr)
            (when (and (bordeaux-threads:thread-alive-p thr)
                       (string= (bordeaux-threads:thread-name thr)
                                "Node socket talker"))
              (log-message :info "Killing Node socket talker thread.")

              (ignore-errors (bordeaux-threads:destroy-thread thr))))
        (bordeaux-threads:all-threads)))


(defun read-message ()
  (declare (special *client*))
  (let* ((msg-length 
          (let ((len (make-array 10 :fill-pointer 0 :element-type 'character))
                (char))
            (loop until (eq (setf char (read-char *client*)) #\#) do
                 (vector-push-extend char len))
            (parse-integer len)))
         (seq (make-array msg-length :element-type 'character)))
    
    (read-sequence seq *client*)
    (log-message :debug-json "JSON object received: ~a~%" seq)

    (let ((msg (with-input-from-string (stream seq)

                 (cl-json:decode-json stream))))
      (log-message :debug-in-messages "Message received: ~a~%" msg)
      msg)))

(defun write-message (message)
  (declare (special *client*))
  (log-message :debug "Sending message: ~s~%" message)  
  (let ((json-string (with-output-to-string (json) (cl-json:encode-json message json))))
    (log-message :debug-json "Stream created: ~s" json-string)
    ;; Extra format as workaround some bug somewhere, probably in iolib
    (let ((val (format nil "~d#~a" (length json-string) json-string)))
      (format *client* val)))
  (finish-output *client*)
  
  (log-message :debug-out-messages "Written message: ~s~%" message))

(defun start-msg-sender ()
  (bordeaux-threads:make-thread #'msg-sender :name "Node socket talker"))

(defun start-msg-receiver ()
  (msg-receiver))

(defun msg-sender ()
  (log-message :info "msg-sender started...")
  (loop
     (handler-case (write-message (pop-queue *message-output*))
       (cl-json:unencodable-value-error (e)
         (log-message :node-error "This json value cannot be encoded: ~s~%"
                      (type-error-datum e))))))

;;; Loops around read message
(defun msg-receiver ()
  (log-message :info "msg-receiver started...")
  (loop
     (handler-case (message-dispatch (read-message))
       (unexhaustive-fcase-definition (e)
         (log-message :node-error "Node invoked non-existent command \"~a\"." (message e)))
       (missing-cmd-from-node ()
         (log-message :node-error "Node sent request without command.")))))



(defun send-message (message &optional params)
  (push-queue (list (cons :c message) (cons :d params)) *message-output*))

(defun message-dispatch (message)
  (msg-dispatcher message))



(defmacro defmessage-out (name (&rest params))
  `(defun ,name (,@params)
     (send-message ',name
                   (list ,@(mapcar (lambda (param)
                                     (list 'cons (intern (symbol-name param) 'keyword)
                                           param))
                                   params)))))

(defmessage-out hello-node ())

(defmacro defmessage-in (msg-name (&rest params) &body b)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel)
       
       (defun ,msg-name (&key ,@params) ,@b)
       (defvar *messages* nil)
       (setf *messages* 
             (delete ,(symbol-name msg-name) *messages*
                     :key #'car
                     :test #'equal))
       (push (list ,(symbol-name msg-name) #',msg-name) *messages*))
       
     (eval-when (:compile-toplevel :execute)
       (format t "Redefining msg-dispatcher with messages: ~s.~%" *messages*)
       (defun msg-dispatcher (msg-obj)

         (let* ((cmd (camel-case-to-lisp (cdr (assoc :c msg-obj)))))
           (unless cmd (error 'missing-cmd-from-node))
           (apply (fcase-symbol-apply cmd *messages*)
                  (alist-plist (cdr (assoc :d msg-obj)))))))))


(defmacro fcase-symbol-apply (var clauses)
  `(fcase-symbol ,var ,@(eval clauses)))



;;; Like fcase but converts strings to uppercase strings and symbols to their uppercase names
(defmacro fcase-symbol (var &rest clauses)
  (once-only (var)
    (with-gensyms (var-str)
      `(let ((,var-str (if (symbolp ,var)
                           (string-upcase (symbol-name ,var))
                           (string-upcase ,var))))

         (fcase ,var-str
                ,@(map 'list
                       (lambda (val)
                         (list* (if (symbolp (first val))
                                    (string-upcase (symbol-name (first val)))
                                    (string-upcase (first val)))
                                (rest val)))
                       clauses))))))


(define-condition missing-cmd-from-node (error) ())

(define-condition no-fcase-defined (error)
  ((err-msg :initform "There are no defined messages, so \"~a\" doesn't have defined function."
            :reader err-msg)))

(define-condition unexhaustive-fcase-definition (error)
  ((message :initarg :message
            :reader message)))

;;; Macro for fast (maybe slow) implementation of switch where we know the string values beforehand
(defmacro fcase (var &rest clauses)
  (once-only (var)
    `(fcase-inner ,var ,@clauses)))

(defmacro fcase-inner (var &rest clauses)
  (if (= (length clauses) 0)
        `(error 'no-fcase-defined)
        (if (= 1 (length clauses))
            `(if (string= ,var ,(first (first clauses)))
                 (progn ,@(rest (first clauses)))
                 (error 'unexhaustive-fcase-definition :message ,var))
            (let* ((sorted-clauses (sort clauses #'string< :key #'car))
                   (division-point (floor (/ (length sorted-clauses) 2)))
                   (left-clauses (subseq sorted-clauses 0 division-point) )
                   (right-clauses (subseq sorted-clauses division-point)))

              `(if (string< ,var ,(first (first right-clauses)))
                   (fcase-inner ,var ,@left-clauses)
                   (fcase-inner ,var ,@right-clauses))))))
