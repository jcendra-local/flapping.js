(in-package :flapping)


(defstruct (cleaners-semaphore (:constructor make-cleaneres-semaphore))
  (guest-counter 0 :type number)
  (guest-card-lock (make-lock) :type sb-thread:mutex)
  (cleaner-card-condition-variable (make-condition-variable :name "Cleaner waiting area")
                                   :type sb-thread:waitqueue)
  (guest-counter-lock (make-lock)  :type sb-thread:mutex))


(defmacro with-room-cleaning ((lock) &body b)
  (once-only (lock)
    `(unwind-protect
          (progn
            (enter-as-cleaner ,lock)
            ,@b)
       (leave-as-cleaner ,lock))))

(defmacro with-room-guest ((lock) &body b)
  (once-only (lock)
    `(unwind-protect
          (progn
            (enter-as-guest ,lock)
            ,@b)
       (leave-as-guest ,lock))))

(defun enter-as-cleaner (semaphore)
  (declare (type cleaners-semaphore semaphore))
  (progn
    (acquire-lock (cleaners-semaphore-guest-card-lock semaphore))
    (loop until (%try-acquire-cleaner-access semaphore))))

(defun leave-as-cleaner (semaphore)
  (declare (type cleaners-semaphore semaphore))
  (release-lock (cleaners-semaphore-guest-counter-lock semaphore))
  (release-lock (cleaners-semaphore-guest-card-lock semaphore)))


(defun %try-acquire-cleaner-access (semaphore)
  (acquire-lock (cleaners-semaphore-guest-counter-lock semaphore))
  (do ()
      ((= 0 (cleaners-semaphore-guest-counter semaphore)) t)
    (or (condition-wait (cleaners-semaphore-cleaner-card-condition-variable semaphore)
                        (cleaners-semaphore-guest-counter-lock semaphore))
        (return-from %try-acquire-cleaner-access nil))))

(defun enter-as-guest (semaphore)
  (declare (type cleaners-semaphore semaphore))
  (with-lock-held ((cleaners-semaphore-guest-card-lock semaphore))
    (%inc-guest-counter semaphore)))

(defun leave-as-guest (semaphore)
  (declare (type cleaners-semaphore semaphore))
  (%dec-guest-counter semaphore))


(defun %inc-guest-counter (semaphore &key (count 1))
  (declare (type cleaners-semaphore semaphore))
  (with-lock-held ((cleaners-semaphore-guest-counter-lock semaphore))
    (incf (cleaners-semaphore-guest-counter semaphore) count)))

(defun %dec-guest-counter (semaphore &key (count 1))
  (declare (type cleaners-semaphore semaphore))
  (with-lock-held ((cleaners-semaphore-guest-counter-lock semaphore))
    (decf (cleaners-semaphore-guest-counter semaphore) count)
    (when (<= (cleaners-semaphore-guest-counter semaphore) 0)
      (setf (cleaners-semaphore-guest-counter semaphore) 0)
      (condition-notify (cleaners-semaphore-cleaner-card-condition-variable semaphore)))
    (cleaners-semaphore-guest-counter semaphore)))





