(in-package :flapping)


(def-suite dll :description "Testing of DLL functionality")
(in-suite dll)


;;; Simple test for creating cell and querying its object
(test create-dll-cell
  (let ((cell (dll-make-cell 'my-obj)))
    (is (eq (dll-obj cell) 'my-obj))
    (setf (dll-obj cell) 'my-other-obj)
    (is (eq (dll-obj cell) 'my-other-obj))))


;;; Adding two cells and iterating list, checking number of iterations and content
(test dll-add-and-iterate
  (let ((head (dll-make-head-n-tail))
        (cell-1 (dll-make-cell 'cell-1))
        (cell-2 (dll-make-cell 'cell-2)))
    (dll-iterate (obj head)
      (fail "dll-iterate ran with empty DLL"))
    (dll-add cell-1 head)

    (let ((num-runs 0))
      (dll-iterate (obj head)
        (is (eq obj 'cell-1)))
      (is (= num-runs 0)))

    (let ((results nil)
          (num-runs 0))

      ;; adding cell 2 on the beginning
      (dll-add cell-2 head)

      (dll-iterate (obj head)
        (incf num-runs)
        ;; Push into list reverses order again, so results contains cells in order they were added
        (push obj results))

      
      (is (= (length results) 2))
      (is (eq (first results)
              'cell-1))
      (is (eq (second results)
              'cell-2))
      (is (= num-runs 2)))))

(test dll-remove-from-list
  (let ((head (dll-make-head-n-tail))
        (cells nil))

    (loop for i from 1 to 10 do
         (let ((cell (dll-make-cell i)))
           (push cell cells)
           (dll-add cell head)))

    (setf cells (nreverse cells))
    
    (dll-remove (fifth cells))

    (let ((num-runs 0))
      (dll-iterate (obj head)
        (incf num-runs)
        (is (<= 1 obj 10))
        (is (/= obj 5)))
      (is (= num-runs 9)))

    (dll-remove (first cells))
    (let ((num-runs 0))
      (dll-iterate (obj head)
        (incf num-runs)
        (is (<= 2 obj 10))
        (is (/= obj 5)))
      (is (= num-runs 8)))

    (dll-remove (car  (last cells)))
    
    (let ((num-runs 0))
      (dll-iterate (obj head)
        (incf num-runs)
        (is (<= 2 obj 9))
        (is (/= obj 5)))
      (is (= num-runs 7)))
    ))

(test dll-remove-only-element
  (let ((head (dll-make-head-n-tail))
        (cell (dll-make-cell 'cell-1)))
    (dll-add cell head)
    (dll-remove cell)
    (dll-iterate (v head)
      (fail "Iteration on through empty DLL"))

    (pass "Test passed")))

(test dll-update-betwween-dlls
  (let ((dll-1 (dll-make-head-n-tail))
        (dll-2 (dll-make-head-n-tail))
        (cells-1 nil)
        (cells-2 nil))

    (loop for i from 1 to 10 do
         (let ((cell (dll-make-cell (+ 100 i))))
           (dll-add cell dll-1)
           (push cell cells-1)))

    (loop for i from 1 to 10 do
         (let ((cell (dll-make-cell (+ 200 i))))
           (dll-add cell dll-2)
           (push cell cells-2)))

    (setf cells-1 (nreverse cells-1))
    (setf cells-2 (nreverse cells-2))

    (dll-update (fifth cells-1) dll-2)


    (let ((num-runs 0))
      (dll-iterate (obj dll-1 (is (= num-runs 9)))
        (incf num-runs)
        (when (= obj 105)
          (fail))))

    (let ((num-runs 0)
          (was-105 nil))
      (dll-iterate (obj dll-2 (progn (is (= num-runs 11))
                                      (is-true was-105)))
        (incf num-runs)
        (setf was-105 (or was-105
                          (= obj
                             105)))))

    (dll-update (first cells-2) dll-1)
    
    (let ((was-201 nil)
          (was-105 nil))
      (let ((num-runs 0))
        (dll-iterate (obj dll-1 (is (= num-runs 10)))
          (incf num-runs)
          (when (= obj
                   201)
            (is (= num-runs 1)))

          (setf was-201 (or was-201
                            (= obj
                               201)))))

      (is-true was-201)

      (let ((num-runs 0))
        (dll-iterate (obj dll-2 (is (= num-runs 10)))
          (incf num-runs)
          (when (= obj 201)
            (fail))
          
          (setf was-105
                (or was-105
                    (= obj
                       105))))))))

