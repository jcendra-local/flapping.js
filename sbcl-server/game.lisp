;;;; game.lisp

(in-package #:flapping)

(defparameter *num-map-kernel-workers* 4)
(defparameter *num-update-segments* 4)

(defvar *kernel* nil)

(defparameter *game-queue* (make-queue))
(defparameter *objs-for-reinsertion-queue* (make-queue))
(defparameter *user-channel* nil)
(defparameter *update-channel* nil)
(defparameter *grid* nil)

(defparameter *users* nil)

(defparameter *game-tick-timer* (sb-ext:make-timer
                                 (lambda ()
                                   (push-queue '(:tick-game) *game-queue*))
                                 :name "Tick timer"
                                 :thread t))

(defparameter *users-to-add* nil)

(defparameter *birds-to-add* nil)

(defparameter *game-loop-running* nil)
(defparameter *game-loop-running-lock* (make-lock))
(defparameter *game-loop-thread* nil)

(defparameter *birds-cleaners-semaphore* (make-cleaneres-semaphore))


(defun initialize-game ()
  (setf *kernel* (lparallel:make-kernel *num-map-kernel-workers*
                                                :name "map-kernel"))

  (setf *game-queue* (make-queue))
  (setf *objs-for-reinsertion-queue* (make-queue))
  (setf *user-channel* (make-channel))
  (setf *update-channel* (make-channel))
  (setf *grid* (make-instance 'grid :game-side 10000 :tile-size 75))
  (setf *users* nil)
  (setf *users-to-add* nil))

(defun start-timer ()
  (sb-ext:schedule-timer *game-tick-timer*
                         0 :repeat-interval 1/60))

(defun stop-timer ()
  (sb-ext:unschedule-timer *game-tick-timer*))

(defun tick-objects (tile)
  (loop-objects-on-tile (obj tile)
       (tick obj *grid*))) 

(defun update-tile-objects (task-index reinsert-dispatch begin end)

  ;; Updating objects
  (dolist (moved-obj (loop for i from begin to end
                        with objects-for-moving = nil
                        finally (return objects-for-moving)
                        do
                          (loop-objects-on-tile (obj (aref (tiles *grid*) i))
                             (when-let ((moving-obj (update-object obj *grid*)))
                               (push moving-obj objects-for-moving)))))

    (log-message :debug-grid "Reinserting objects...")

    ;; Reinserting objects in same chung
    (with-accessors ((x x) (y y) (tile tile)) moved-obj
      (if (<= begin (index (tile-from-coord *grid* x y)) end)
          (grid-update *grid* moved-obj x y)
          (progn
            (grid-remove moved-obj)
            (funcall reinsert-dispatch moved-obj)))))

  (list :updating-finished task-index))

(defun reinsert-objects (task-index reinsert-objects-queue)
  (loop
     (let ((val (pop-queue reinsert-objects-queue)))
       (when (eq val :updating-finished) (return))
       (grid-store val (x val) (y val) *grid*)))
  (loop
     (multiple-value-bind (obj val-p) (try-pop-queue reinsert-objects-queue)
       (when (eq val-p nil) (return))
       (grid-store obj (x obj) (y obj) *grid*)))
  (list :reinserting-finished task-index))

(defun make-reinsert-dispatch (chunks reinsert-queues)
  (lambda (obj)
    (push-queue obj
                (nth (the integer
                          (loop with index = (index (tile-from-coord *grid* (x obj) (y obj)))
                             for chunk in chunks for i from 0
                             thereis (and (<= (car chunk) index (cdr chunk)) i)
                             finally (error "Chunk not found.")))
                     reinsert-queues))))

(defun give-user-view-task (user)
  (log-message :debug-view "Starting get-user-view-task for user ~s" user)
  
  (with-room-guest (*birds-cleaners-semaphore*)
    (let ((birds (owned-birds user)))

      (when (dll-end-p birds)
        (return-from give-user-view-task))

      (let* ((bird (dll-obj (dll-next birds)))
             (x1 (x bird))
             (y1 (y bird))
             (x2 (x bird))
             (y2 (y bird)))

        (dll-iterate (bird birds)
          (with-accessors ((x x)
                           (y y)) bird
            (when (< x x1) (setf x1 x))
            (when (< y y1) (setf y1 y))
            (when (> x x2) (setf x2 x))
            (when (> y y2) (setf y2 y))))

        (decf x1 40)
        (decf y1 40)
        (incf x2 40)
        (incf y2 40)

        (log-message :debug-view "Found bounding box: x1: ~s y1: ~s x2: ~s y2: ~s" x1 y1 x2 y2)

        ;; (log-message :debug "Temporarily showing the whole map")
        ;; (setf x1 -20)
        ;; (setf x2 10020)
        ;; (setf y1 -20)
        ;; (setf y2 10020)


        (let* ((bounding-box-width (- x2 x1))
               (bounding-box-height (- y2 y1))
               (screen-ratio (/ (screen-width user)
                                (screen-height user)))

               
               (bounding-box-ratio (/ bounding-box-width
                                      bounding-box-height))
               (fitted-height (* bounding-box-height
                                 (max 1 (/ bounding-box-ratio screen-ratio))) )

               (fitted-y (- y1 (/ (- fitted-height bounding-box-height) 2)))

               (fitted-width (* bounding-box-width
                                (max 1 (/ screen-ratio bounding-box-ratio))))
               (fitted-x (- x1 (/ (- fitted-width bounding-box-width) 2)))

               (birds nil))


          (loop-objects-within-rectangle *grid* obj
             fitted-x fitted-y
             (+ fitted-x fitted-width) (+ fitted-y fitted-height)
             (log-message :debug-view "Found obj in rectangle: ~s" obj)
             (log-message :debug-view "obj id: ~s" (id obj))
             (log-message :debug-view "user: ~s" user)

             (push 
              (cons (id obj)
                    (pairlis '(:id :x :y :d-x :d-y :hue)
                             (list (id obj)
                                   (round (x obj))
                                   (round (y obj))
                                   (dx obj)
                                   (dy obj)
                                   #x00FFFF

                                   
                                   ;; (pairlis '(:sum-pos-x :sum-pos-y :sum-pos-weights :sum-dir-x :sum-dir-y :sum-dir-weights :sum-sep-x :sum-sep-y :force-x :force-y :angle-middle)
                                   ;;          (list (sum-pos-x obj)
                                   ;;                (sum-pos-y obj)
                                   ;;                (sum-pos-weights obj)
                                   ;;                (sum-dir-x obj)
                                   ;;                (sum-dir-y obj)
                                   ;;                (sum-dir-weights obj)
                                   ;;                (sum-sep-x obj)
                                   ;;                (sum-sep-y obj)
                                   ;;                (force-x obj)
                                   ;;                (force-y obj)
                                   ;;                (angle-middle obj)))
                                   )))
              birds))


          (log-message :debug-view "Sending user view for user ~s" (id user))
          (user-view (id user)
                     (round fitted-x)
                     (round fitted-y)
                     (round fitted-width)
                     (round fitted-height)
                     birds))))))

(defun get-user-from-id (user-id)
  (find user-id *users* :key (lambda (user) (id user))
        :test #'equal))

(defun give-user-view (user-id)
  (let ((user (get-user-from-id user-id)))
    (when user
        (submit-task *user-channel*
                     #'give-user-view-task
                     user))))

(defun add-user (user-id)
  (log-message :debug "Adding user ~a" user-id)
  (let ((new-user (make-instance 'user :id user-id))
        (new-bird (make-instance 'bird)))
    (push new-user *users-to-add*)
    (setf (owner new-bird) new-user)
    (dll-add (owner-cell new-bird)
             (owned-birds new-user))))

(defun resize-user-screen (user-id width height)
  (let ((user (get-user-from-id user-id)))
    (when user
      (resize-screen user width height))))

(defun add-bird (user-id)
  (let ((bird (make-instance 'bird)))
    (setf (owner bird) (get-user-from-id user-id))
    (push bird *birds-to-add*)))

(defun game-loop ()
  (with-lock-held (*game-loop-running-lock*)
    (setf *game-loop-running* t))

  (do ((continue-loop t)
       (msg (pop-queue *game-queue*) (and continue-loop (pop-queue *game-queue*)))
       (loop-count 0 (1+ loop-count)))
      ((not continue-loop) loop-count)

    (log-message :debug "MSG: ~a" msg)

    (fcase-symbol (first msg)
                  (:give-user-view (give-user-view (second msg)))
                  (:add-user (add-user (second msg)))
                  (:tick-game (tick-game))
                  (:screen-resized (apply #'resize-user-screen (rest msg)))
                  (:add-bird-to-user (apply #'add-bird (rest msg)))
                  (:stop-game-loop (setf continue-loop nil)))
    (try-receive-result *user-channel*))

  (with-lock-held (*game-loop-running-lock*)
    (setf *game-loop-running* nil)))

(defun game-loop-running-p ()
  (with-lock-held (*game-loop-running-lock*)
    *game-loop-running*))

(defun tick-game ()
  (with-room-guest (*birds-cleaners-semaphore*)
      (pdotimes (i (length (tiles *grid*)) nil *num-update-segments*)
        (tick-objects (aref (tiles *grid*) i))))

  (log-message :debug-tick "Ticking game")

  ;; wait for readers to go away
  (with-room-cleaning (*birds-cleaners-semaphore*) 

    ;; split grid to n segments
    (let* ((chunks (get-arr-chunks (length (tiles *grid*)) ;split grid size to segments
                                   *num-update-segments*))
           (reinsert-queues (loop for i from 0 to (1- *num-update-segments*) collect
                                 (make-queue)))
           (reinsert-dispatch (make-reinsert-dispatch chunks reinsert-queues)))

      (log-message :debug-tick "Updating tile objects")
      (dotimes (i *num-update-segments*)
        (submit-task *update-channel* #'update-tile-objects
                     i
                     reinsert-dispatch
                     (car (nth i chunks))
                     (cdr (nth i chunks))))

      (let ((finished-updating-workers 0)
            (finished-reinserting-workers 0))
        (log-message :debug-tick "Turnaround for reinserting objects")

        (loop until (>= finished-updating-workers *num-update-segments*) do
             (let* ((result (receive-result *update-channel*))
                    (task (first result))
                    (task-id (second result)))
               (ecase task
                 (:updating-finished (incf finished-updating-workers)
                                     (log-message :debug-tick
                                                  "Updating of task ~s finished, submitting reinserting task"
                                                  task-id)
                                     (submit-task *update-channel* #'reinsert-objects
                                                  task-id
                                                  (nth task-id reinsert-queues)))
                 (:reinserting-finished (incf finished-reinserting-workers)
                                        (log-message :debug-tick
                                                     "Reinserting task ~s finished before updating of all"
                                                     task-id)))))

        (log-message :debug-tick "Notifying about updating being finished")
        (dolist (queue reinsert-queues)
          (push-queue :updating-finished queue))

        (loop until (>= finished-reinserting-workers *num-update-segments*) do
             (log-message :debug-tick "Waiting for reinsertion to complete")
             (let* ((result (receive-result *update-channel*))
                    (task (first result))
                    (task-id (second result)))
               (assert (eql task :reinserting-finished))
               (incf finished-reinserting-workers)
               (log-message :debug-tick "Reinsertion task ~a is complete" task-id)))))
    
    (log-message :debug-tick "Reinsertion complete, adding new users.")

    ;; Add users that are waiting for birds
    (loop while *users-to-add* do
         (let ((new-user (pop *users-to-add*)))
           (push new-user *users*)
           (dll-iterate (bird (owned-birds new-user))
             (grid-store bird 500 500 *grid*))))

    (log-message :debug-tick "Adding users complete, adding birds")

    (loop while *birds-to-add* do
         (let ((bird (pop *birds-to-add*)))
           (dll-add (owner-cell bird)
                    (owned-birds (owner bird)))

           (setf (x bird) (random 1000))
           (setf (y bird) (random 1000))
           (grid-store bird
                       (x bird)
                       (y bird)
                       *grid*)))) 
  
  (log-message :debug-tick "Game tick complete")) 

(defun get-arr-chunks (size parts)
  (let ((chunks (multiple-value-bind (quot rem) (floor size parts)
                  (append (make-list rem :initial-element (1+ quot))
                          (make-list (- parts rem) :initial-element quot))))
        (result '())
        (start 0))
    
    (dolist (chunk chunks (nreverse result))
      (push (cons start (1- (+ start chunk))) result)
      (incf start chunk))))

