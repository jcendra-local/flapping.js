;;;; flapping.asd

(in-package :cl-user)



(asdf:defsystem #:flapping
  :description "Describe flapping here"
  :author "Your Name <your.name@example.com>"
  :license "Specify license here"
  :depends-on (#:lparallel
               #:iolib
               #:alexandria
               #:cl-log
               #:cl-json
               #:fiveam)
  :serial t
  :components ((:file "package")
               (:file "config" :depends-on ("package"))
               (:file "listener" :depends-on ("config"))
               (:file "dll" :depends-on ("config"))
               (:file "cleaners-semaphore")
               (:file "grid" :depends-on ("dll"))
               (:file "bird" :depends-on ("grid"))
               (:file "user" :depends-on ("dll"))
               (:file "game" :depends-on ("listener" "grid" "bird"))
               (:file "flapping" :depends-on ("game"))
               (:module tests
                        :depends-on ("flapping")
                        :components ((:file "dll-test")))))


