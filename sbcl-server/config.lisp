(in-package :flapping)

(declaim (optimize (safety 3)
                   (debug 3)
                   (compilation-speed 0)
                   (space 0)
                   (speed 0)))


;; (declaim (optimize (safety 0)
;;                    (debug 0)
;;                    (compilation-speed 0)
;;                    (space 0)
;;                    (speed 3)))




(setf *print-circle* t)

(defcategory :debug-tick)
(defcategory :debug-json)
(defcategory :debug-in-messages)
(defcategory :debug-out-messages)
(defcategory :debug-messages (or :debug-messages
                                 :debug-in-messages
                                 :debug-out-messages))

(defcategory :debug-readlock)
(defcategory :debug-view)

(defcategory :debug-flocking)

(defcategory :node-error)


(defcategory :critical)
(defcategory :error (or :error :critical :node-error))
(defcategory :warning (or :warning :error))
(defcategory :notice (or :notice :warning))
(defcategory :info)
(defcategory :debug (or :debug :info :notice))


(defparameter *log-level* '(or :info))
(defparameter *log-stream* *standard-output*)

