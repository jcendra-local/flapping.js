(in-package :flapping)


;;; TODO turn those into setfable functions
(defmacro dll-prev (cell)
  `(cadr ,cell))

(defmacro dll-next (cell)
  `(cddr ,cell))

(defmacro dll-next-of-prev (cell)
  `(cddadr ,cell))

(defmacro dll-prev-of-next (cell)
  `(cadddr ,cell))

(defmacro dll-obj (cell)
  `(car ,cell))

(defun dll-make-head-n-tail ()
  (let ((head (cons nil (cons nil nil)))
        (tail (cons nil (cons nil nil))))
    (setf (dll-next head) tail)
    (setf (dll-prev tail) head)
    head))

(defmacro dll-iterate ((var head &optional return-form) &body b)
  (with-gensyms (cell)
    `(do* ((,cell (dll-next ,head)      ;starting after head
                  (dll-next ,cell))
           (,var (dll-obj ,cell)
                 (dll-obj ,cell)))
          ((null (dll-next ,cell)) ,return-form) ;test that we're on end
       ,@b)))


(defun dll-make-cell (obj)
  (cons obj (cons nil nil)))

(defun dll-add (cell head)
  (setf (dll-prev cell) head)
  (setf (dll-next cell) (dll-next head))
  (setf (dll-next head) cell)
  (setf (dll-prev-of-next cell) cell))

(defun dll-remove (cell)
  (setf (dll-next-of-prev cell) (dll-next cell))
  (setf (dll-prev-of-next cell) (dll-prev cell))
  ;; Clearing up to help garbage collector, dunno if it actually helps
  (setf (dll-prev cell) nil)
  (setf (dll-next cell) nil))

(defun dll-update (cell head)
  (setf (dll-next-of-prev cell) (dll-next cell))
  (setf (dll-prev-of-next cell) (dll-prev cell))
  (dll-add cell head))

(defun dll-end-p (cell)
  (null (dll-next (dll-next cell))))
