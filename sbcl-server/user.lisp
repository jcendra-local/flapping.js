(in-package :flapping)


(defclass user ()
  ((id :initarg :id
       :reader id)
   (owned-birds :accessor owned-birds
                :initform (dll-make-head-n-tail))
   (hue :type fixnum)
   (screen-width :initform 0
                 :reader :screen-width)
   (screen-height :initform 0
                  :reader :screen-height)))

(defgeneric resize-screen (user width height)
  (:method ((user user) width height)
    (with-slots (screen-width screen-height) user
      (setf screen-width width)
      (setf screen-height height))))

(defgeneric remove-user (obj)
  (:method ((bird bird))
    (with-lock-held (bird)
      (dll-remove (owner-cell bird)))))

(defgeneric remove-obj (user obj)
  (:method ((user user) (obj bird))))

(defgeneric add-obj (owner obj)
  (:method ((user user) (bird bird))
    (dll-add (owner-cell bird)
             (owned-birds user))))




