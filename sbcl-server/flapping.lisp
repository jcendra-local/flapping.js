;;;; flapping.lisp

(in-package #:flapping)

(defparameter *server-running* nil)
(defparameter *server-thread* nil)


;;; Opens socket and starts listening
(defun start-server ()
  (assert (not *server-running*))
  
  (setf *server-running* t)
  (start-logger)
  (initialize-game)
  (run-listener)
  (start-timer)
  (start-game-loop))

(defun restart-log-messenger ()
  (stop-messenger (car (log-manager-messengers (log-manager))))
  (start-messenger 'text-stream-messenger
                   :name "flapping-messenger"
                   :stream *standard-output*
                   :filter *log-level*))


(defun start-logger ()
  (setf (log-manager) (make-instance 'log-manager :message-class 'formatted-message))
  (start-messenger 'text-stream-messenger
                   :name "flapping-messanger"
                   :stream *standard-output*
                   :filter *log-level*))

(defun stop-server ()
  (stop-timer)
  (stop-game-loop)
  (ignore-errors (destroy-thread *connection-thread*))
  (lparallel:end-kernel)
  (setf *server-running* nil))

(defun restart-server ()
  (stop-server)
  (start-server))
  

(defun start-game-loop ()
  (setf *game-loop-thread* 
        (make-thread #'game-loop :name "Game loop")))


(defun stop-game-loop ()
  (push-queue '(:stop-game-loop) *game-queue*))


(defmessage-out user-view (user-id x y w h birds))

(defmessage-in screen-resized (user-id w h)
  (push-queue (list :screen-resized user-id w h)
              *game-queue*))

(defmessage-in user-connected (user-id)
  (push-queue (list :add-user user-id) *game-queue*)
  
  (log-message :info "User with id ~a connected." user-id)
  (finish-output))

(defmessage-in request-user-view (user-id)
  (push-queue (list :give-user-view user-id) *game-queue*))

(defmessage-in user-input (user-id input-x input-y))

(defmessage-in hey-there ()
  (log-message :info "Node's saying Hey There!"))

(defmessage-in heartbeat ()
  (log-message :debug-messages "Heartbeat received. Responding.")
  ;; (send-message :heart-beat)
  (send-message :log-this '((:msg . "Heartbeat response"))))

(defmessage-in add-bird-to-user (user-id)
  (push-queue (list :add-bird-to-user user-id) *game-queue*))


