(in-package :flapping)

(defclass tile ()
  ((lock :initform (make-lock "Tile lock")
         :accessor lock)
   (head :initform (dll-make-head-n-tail)
         :accessor head)
   (index :initarg :index
          :reader index)))


(defparameter *map-obj-ids* 0)
;;; Object in a map, zero size for now... 
(defclass map-object ()
  ((id :reader id
       :initform (incf *map-obj-ids*))
   (x :initform 0 :type fixnum :accessor x)
   (y :initform 0 :type fixnum :accessor y)
   (dx :initform 0 :type fixnum :accessor dx)
   (dy :initform 0 :type fixnum :accessor dy)
   (new-dx :initform 0 :type fixnum :accessor new-dx)
   (new-dy :initform 0 :type fixnum :accessor new-dy)
   (tile-cell :initform nil
              :accessor tile-cell)
   (tile :initform nil
         :accessor tile)
   (read-lock :initform (make-lock)
              :accessor read-lock)
   (write-lock :initform (make-lock)
               :accessor write-lock)))



(defgeneric is-in-map (map-object)
  (:method ((obj map-object))
    (not (null (tile obj)))))

(defmethod initialize-instance :after ((obj map-object) &key)
  (setf (tile-cell obj) (dll-make-cell obj)))

(defclass grid ()
  ((tiles :accessor tiles)
   (multi-tile-lock :initform (make-lock "Multitile Lock")
                    :reader multi-tile-lock)
   (grid-size :reader grid-size
              :initarg :grid-size)
   (game-side :initarg :game-side
              :reader game-side)
   (tile-size :initarg :tile-size :reader tile-size)))


(defgeneric tile-from-coord (grid x y)
  (:method ((grid grid) x y)
    (with-accessors ((tiles tiles)) grid
      (aref tiles (get-z-coord (limit-coord x grid)
                               (limit-coord y grid)
                               grid)))))

(defmethod initialize-instance :after ((grid grid) &key game-side tile-size)
  (with-slots (grid-size tiles) grid
    (setf grid-size (expt 4 (ceiling (log (ceiling ( / game-side tile-size )) 2))))
    (setf tiles (make-array grid-size :initial-element nil))

    ;; Fill tiles with tile objects
    (loop for i from 0 to (1- grid-size) do
         (setf (aref (tiles grid) i)
               (make-instance 'tile :index i)))))

(defun morton-encode (x y)
  (declare (fixnum x y))
  (logior (split-int x) (ash (split-int y) 1)))

(defun split-int (x)
  (declare (fixnum x))
  (setq x (logand x #x0000ffff))
  (setq x (logand (logxor x (ash x 8)) #x00ff00ff))
  (setq x (logand (logxor x (ash x 4)) #x0f0f0f0f))
  (setq x (logand (logxor x (ash x 2)) #x33333333))
  (setq x (logand (logxor x (ash x 1)) #x55555555)))


(defgeneric limit-coord (coord grid)
  (:method (coord (grid grid))
      (max 0 (min (game-side grid) coord))))

(defgeneric get-z-coord (x y grid)
  (:method (x y (grid grid))
    (with-accessors ((tile-size tile-size)) grid
      (morton-encode (floor (/ x tile-size))
                     (floor (/ y tile-size))))))

(defgeneric tile-at (z-coord grid)
  (:method (z-coord (grid grid))
    (aref (tiles grid) z-coord)))

(defgeneric tile-store (tile obj)
  (:method ((tile tile) (obj map-object))
    (with-accessors ((head head) (lock lock)) tile
      (with-lock-held (lock)
        (dll-add (tile-cell obj) head)))))

(defgeneric tile-remove (tile obj)
  (:method ((tile tile) (obj map-object))
    (with-lock-held ((lock tile))
      (dll-remove (tile-cell obj)))))

(defgeneric grid-store (obj x y grid)
  (:method (obj x y (grid grid)) 
    (with-lock-held ((write-lock obj))
      (let ((tile (tile-from-coord grid x y)))
        (setf (x obj) x)
        (setf (y obj) y)
        (setf (tile obj) tile)
        (tile-store tile obj)))))

(defun grid-remove (obj)
  (with-lock-held ((write-lock obj))
    (tile-remove (tile obj) obj)
    (setf (tile obj) nil)))

(defgeneric grid-update (grid obj x y)
  (:method ((grid grid) (obj map-object) x y)
    (with-accessors ((obj-x x) (obj-y y) (obj-tile tile)) obj
      (let ((old-tile (tile obj))
            (new-tile (tile-from-coord grid x y)))
        (unwind-protect
             (progn  (with-lock-held ((multi-tile-lock grid))
                       (acquire-lock (write-lock obj))
                       (acquire-lock (lock new-tile))
                       (acquire-lock (lock old-tile))))
          (setf obj-x x)
          (setf obj-y y)
          (setf (tile obj) new-tile)
          (dll-update (tile-cell obj)
                      (head new-tile))
          (release-lock (write-lock obj))
          (release-lock (lock new-tile))
          (release-lock (lock old-tile)))))))

(defmacro loop-objects-within-rectangle (grid var x1-form y1-form x2-form y2-form &body body)
  (once-only (grid)
    (with-gensyms (x y x1 y1 x2 y2 tile-size)
      `(let  ((,x1 (limit-coord ,x1-form ,grid))
              (,y1 (limit-coord ,y1-form ,grid))
              (,x2 (limit-coord ,x2-form ,grid))
              (,y2 (limit-coord ,y2-form ,grid)))
         (with-accessors ((,tile-size tile-size)) ,grid
           (loop for ,x
              from (* ,tile-size (floor (/ ,x1 ,tile-size)))
              to (* ,tile-size (floor (/ ,x2 ,tile-size)))
              by ,tile-size
              do
                (loop for ,y
                   from (* ,tile-size (floor (/ ,y1 ,tile-size)))
                   to (* ,tile-size (floor (/ ,y2 ,tile-size)))
                   by ,tile-size
                   do
                     (loop-objects-on-tile (,var (tile-from-coord ,grid ,x ,y))
                        ,@body))))))))

(defmacro loop-objects-within-distance (grid var x y radius &body body)
  "Iterated value is in `var`"
  (once-only (grid x y radius) 
    (with-gensyms (rad-sqr)
      `(let ((,rad-sqr (expt ,radius 2)))
         (loop-objects-within-rectangle ,grid ,var (- ,x ,radius) (- ,y ,radius) (+ ,x ,radius) (+ ,y ,radius)
            (when (<= (+ (expt (- ,x (x ,var)) 2)
                         (expt (- ,y (y ,var)) 2))
                      ,rad-sqr)
              ,@body))))))


(defmacro loop-objects-on-tile ((var tile &optional return-form) &body b)
  `(dll-iterate (,var  (head ,tile) ,return-form)
     ,@b))





