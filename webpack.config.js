var path = require('path');
module.exports = {
    entry: "/home/therik/flapping.js/src/client/js/app.js",
    output: {
        path: path.resolve("/home/therik/flapping.js/src/client/js"),
        library: "app",
        filename: "app.js"
    },
    
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel'
            },
            {
                test: /\.json$/,
                loader: 'json'
            }
        ],

        postLoaders: [
            {
                include: path.resolve(__dirname, 'node_modules/pixi.js'),
                loader: 'transform?brfs'
            }
        ]

        // ,
        // noParse: [ /.*(pixi\.js).*/ ]
        
    }
};

