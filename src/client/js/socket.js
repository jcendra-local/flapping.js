var socketIO = require('socket.io-client');

function Socket(Game) {
    this.socket = null;
    this.latency = 0;

    this.connected = false;

    this.start = function() {
        if (!this.socket) {
            this.socket = socketIO();
            console.log("Starting socket..");
            this.connected = true;
        }

        this.ping();

        this.socket.on('connect_failed', () => {
            console.log('Connect failed, closing..');
            this.socket.close();
            this.connected = false;
        });

        this.socket.on('disconnect', () => {
            console.log('Received disconnect request, closing..');
            this.socket.close();
            this.connected = false;
        });

        this.socket.on('respawned', Game.onRespawned.bind(Game));
        this.socket.on('getSceneSize', Game.sendSceneSize.bind(Game));
        this.socket.on('serverTellPlayerMove', Game.tellPlayerMove.bind(Game));
    };

    this.ping = function () {
        console.log('Pinging..');
        let promise = new Promise((succ, fail) => {
            let pingStartTime = Date.now();

            this.socket.once('pong', () => {
                succ(Date.now() - pingStartTime);
            });

            window.setTimeout(() => {
                fail();
            }, 10000);

            
            this.socket.emit('ping');
        });

        promise.then((latency) => {
            this.latency = latency;
            console.log('Ping: ' + latency);
        }, () => {
            this.latency = 9999;
            console.log('Ping request expired!');
        });

        return promise;
    };

    this.createNewPlayer = function(playerName) {
        console.log('Emiting createNewPlayer..');
        this.socket.emit('createNewPlayer', playerName);
    };

    this.respawn = function() {
        console.log('Emiting respawn..');
        this.socket.emit('respawn');
    };

    this.resize = function(width, height) {
        console.log('Emiting resize..');
        this.socket.emit('resize', {screenWidth: width, screenHeight: height});
    };
    
    this.getLatency = function() {
        return this.latency;
    };
    
}

exports.Socket = Socket;
