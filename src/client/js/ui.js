

// Checks if the nick chosen contains valid alphanumeric characters (and underscores).
function validNick(nick) {
    var regex = /^\w*$/;
    return regex.exec(nick) !== null;
}


exports.validNick = validNick;
