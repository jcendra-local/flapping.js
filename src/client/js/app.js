var io = require('socket.io-client');
var global = require('./global');

var game = require('./game.js');
var Game = new game.Game();

window.Game = Game;


window.addEventListener('resize', () => {
    Game.resize(window.innerWidth, window.innerHeight);
});


function startGame(playerName) {
    document.getElementById('startMenuWrapper').style.maxHeight = '0px';
    document.getElementById('gameAreaWrapper').style.opacity = 1;
    Game.start(playerName);
}

window.onload = function() {
    var startBtn = document.getElementById('startButton');
    var playerNameInput = document.getElementById('playerNameInput');

    startBtn.onClick = function () {
        startGame(playerNameInput.value);
    };

    playerNameInput.addEventListener('keypress', function (e) {
        var key = e.which || e.keyCode;

        if (key === global.KEY_ENTER) {
            startGame(playerNameInput.value);
        }
    });

};
