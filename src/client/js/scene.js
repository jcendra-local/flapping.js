var global = require('./global');
var pixi = require('pixi.js');

function Scene(){
    var self = this;

    var textures = {};
    this.renderer = new pixi.autoDetectRenderer(800, 600, {
        view: document.getElementById('game-area'),
        transparent: true,
        antialias: true,
        resolution: 1
        // autoResize: true
        
    });
    // document.getElementById('canvas-container').appendChild(this.renderer.view);
    this.stage = new pixi.Container();
    this.stage.interactive = true;

    
    // this.birdContainer = new pixi.ParticleContainer(15000, {
    //     position: true,
    //     rotation: true,
    //     scale: true
    // });

    this.birdContainer = new pixi.Container();

    
    this.backgroundContainer = new pixi.Container();

    var cloudTexture = pixi.Texture.fromImage("img/cloud-309024.svg");
    this.clouds = new pixi.extras.TilingSprite(cloudTexture, 800, 600);

    this.borders = new pixi.Graphics();
    this.debugVectors = new pixi.Graphics();
    this.debugVectorsTwo = new pixi.Graphics();

    this.debugSumPosVectors = new pixi.Graphics();
    this.debugSumDirVectors = new pixi.Graphics();
    this.debugSumSepVectors = new pixi.Graphics();
    this.debugForceVectors = new pixi.Graphics();
    this.debugAngleMiddle = new pixi.Graphics();


    this.stage.addChild(this.clouds);
    this.stage.addChild(this.borders);
    this.stage.addChild(this.birdContainer);
    this.stage.addChild(this.debugSumPosVectors);
    this.stage.addChild(this.debugSumDirVectors);
    this.stage.addChild(this.debugSumSepVectors);
    this.stage.addChild(this.debugForceVectors);
    this.stage.addChild(this.debugAngleMiddle);
    
    this.birdSprites = {};

    this.boundingBoxX = 0;
    this.boundingBoxY = 0;
    this.boundingBoxWidth = 0;
    this.boundingBoxHeight = 0;

    this.mousePosX = 0;
    this.mousePosY = 0;
    this.mouseClicked = false;
    
    function mouseMove(event){
        self.mousePosX = event.data.global.x;
        self.mousePosY = event.data.global.y;
    }

    function mouseDown(event){
        self.mouseClicked = true;
    }

    function mouseUp(event){
        self.mouseClicked = false;
    }

    this.getMouseXCoords = function(){
        return this.mouseClicked ? xSceneToGlobal(this.mousePosX) : null;
    };

    this.getMouseYCoords = function(){
        return this.mouseClicked ? ySceneToGlobal(this.mousePosY) : null;
    };

    
    this.stage.on('mousemove', mouseMove);
    this.renderer.view.onmousedown = mouseDown;
    this.renderer.view.onmouseup = mouseUp;

    function xGlobalToScene(x){
        // ratio of width of scene to width of box
        var rWS2B = self.getSceneWidth() / self.boundingBoxWidth;
        return Math.round((x - self.boundingBoxX) * rWS2B);
    }

    function yGlobalToScene(y){
        var rHS2B = self.getSceneHeight()  / self.boundingBoxHeight;
        return Math.round((y - self.boundingBoxY) * rHS2B);
    }

    function xSceneToGlobal(x){
        var rWB2S =  self.boundingBoxWidth / self.getSceneWidth();

        return Math.round((x * rWB2S) + self.boundingBoxX);
    }

    function ySceneToGlobal(y){
        var rHB2S = self.boundingBoxHeight / self.getSceneHeight();
        return Math.round((y * rHB2S) + self.boundingBoxY);
    }

    this.getBackgroundOffsetCss = function(){
        var scale = (this.getSceneWidth / this.boundingBoxWidth);
        return (-this.boundingBoxX) + "px " + (-this.boundingBoxY) + "px"; 
    };

    this.getBackgroundSizeCss = function(){
        return (1500 * (this.getSceneWidth / this.boundingBoxWidth)) + "px";
    };

    this.getSceneWidth = function(){
        return this.renderer.view.width;
    };

    this.getSceneHeight = function(){
        return this.renderer.view.height;
    };

    this.drawBackground = function(){
        var scale = self.getSceneWidth() / this.boundingBoxWidth;

        
        var bgX = xGlobalToScene(0);
        var bgY = yGlobalToScene(0);

        // var bgX = - this.boundingBoxX;
        // var bgY = - this.boundingBoxY;
        this.clouds.tilePosition.set(bgX, bgY);
        this.clouds.tileScale.set(scale, scale);
    };

    this.drawBorders = function(){
        this.borders.clear();
        this.borders.lineStyle(Math.max(2, Math.round(50500/this.boundingBoxWidth)), 0xeF0500, 1);

        this.borders.moveTo(xGlobalToScene(0), yGlobalToScene(0));
        this.borders.lineTo(xGlobalToScene(10000), yGlobalToScene(0));
        this.borders.lineTo(xGlobalToScene(10000), yGlobalToScene(10000));
        this.borders.lineTo(xGlobalToScene(0), yGlobalToScene(10000));
        this.borders.lineTo(xGlobalToScene(0), yGlobalToScene(0));
    };

    function setPos(bird, x, y){
        bird.position.x = x;
        bird.position.y = y;
    }

    function setRot(bird, rot){
        bird.rotation = rot;
    }

    function setScale(bird, scale){
        bird.scale.set(scale/10, scale/8);
    }

    this.render = function(){
        this.renderer.render(this.stage);
    };

    function drawBird(birds, birdId, scale){
        setPos(self.birdSprites[birdId], xGlobalToScene(birds[birdId].x), yGlobalToScene(birds[birdId].y));
        setRot(self.birdSprites[birdId], Math.atan2(birds[birdId].dY, birds[birdId].dX) - 3.66519);
        setScale(self.birdSprites[birdId], scale);

    }
    

    this.drawBirds = function(birds){
        var scale =  this.getSceneWidth() / this.boundingBoxWidth;

        this.debugSumPosVectors.clear();
        this.debugSumDirVectors.clear();
        this.debugSumSepVectors.clear();
        this.debugForceVectors.clear();
        this.debugAngleMiddle.clear();


        for(var birdId in birds){
            drawBird(birds, birdId, scale);

            if(birds[birdId].debugValues) {
                this.drawDebugVectors(birds[birdId]);
            }

            // this.birdSprites[birdId].position.x = xGlobalToScene(birds[birdId].x);

            // this.birdSprites[birdId].position.y = yGlobalToScene(birds[birdId].y);
            // this.birdSprites[birdId].rotation = Math.atan2(birds[birdId].dY, birds[birdId].dX) - 3.66519;
            // this.birdSprites[birdId].scale.set(scale/10, scale/8);
        }

        this.render();
    };

    this.drawDebugVectors = function(bird){
        if (bird.debugValues.sumPosWeights > 0) {

            this.debugSumPosVectors.lineStyle(2, 0x000000, 1);
            this.debugSumPosVectors.moveTo(xGlobalToScene(bird.x), yGlobalToScene(bird.y));

            var debugPosX = bird.x + bird.debugValues.sumPosX / bird.debugValues.sumPosWeights;
            var debugPosY = bird.y + bird.debugValues.sumPosY / bird.debugValues.sumPosWeights;

            this.debugSumPosVectors.lineTo(xGlobalToScene(debugPosX), yGlobalToScene(debugPosY));
        }

        if (bird.debugValues.sumDirWeights > 0) {
            this.debugSumDirVectors.lineStyle(2, 0x10b080, 1);
            this.debugSumDirVectors.moveTo(xGlobalToScene(bird.x), yGlobalToScene(bird.y));

            var debugDirX = bird.x + bird.debugValues.sumDirX / bird.debugValues.sumDirWeights;
            var debugDirY = bird.y + bird.debugValues.sumDirY / bird.debugValues.sumDirWeights;

            this.debugSumDirVectors.lineTo(xGlobalToScene(debugDirX), yGlobalToScene(debugDirY));
        }

        // if (bird.sumSepWeights > 0) {
        this.debugSumSepVectors.lineStyle(5, 0xd02020, 1);
        this.debugSumSepVectors.moveTo(xGlobalToScene(bird.x), yGlobalToScene(bird.y));

        var debugSepX = (bird.x + bird.debugValues.sumSepX); // / bird.debugValues.sumSepWeights;
        var debugSepY = (bird.y + bird.debugValues.sumSepY); // / bird.debugValues.sumSepWeights;

            this.debugSumSepVectors.lineTo(xGlobalToScene(debugSepX), yGlobalToScene(debugSepY));
        // }
        
        
        this.debugForceVectors.lineStyle(2, 0x0030c0, 1);
        this.debugForceVectors.moveTo(xGlobalToScene(bird.x), yGlobalToScene(bird.y));
        
        var forceX = bird.x + bird.debugValues.forceX;
        var forceY = bird.y + bird.debugValues.forceY;
        
        this.debugForceVectors.lineTo(xGlobalToScene(forceX), yGlobalToScene(forceY));


        this.debugAngleMiddle.lineStyle(1, 0x000000, 1);
        

        // bird.x
        // bird.y
        // 150

        // bird.dx bird.dy
        
        // angleMiddle + 
        
        var frontX = bird.x + bird.dx * 30;
        var frontY = bird.y + bird.dy * 30;

        var arcX = bird.x + bird.dx * Math.cos(bird.debugValues.angleMiddle) - bird.dy * Math.sin(bird.debugValues.angleMiddle);
        var arcY = bird.y + bird.dx * Math.sin(bird.debugValues.angleMiddle) + bird.dy * Math.cos(bird.debugValues.angleMiddle);

        
        this.debugAngleMiddle.arcTo(xGlobalToScene(frontX), yGlobalToScene(frontY),
                               xGlobalToScene(arcX), yGlobalToScene(arcY),
                               Math.sqrt(Math.pow(bird.dx * 30, 2) + Math.pow(bird.dy * 30, 2)));
    };
    
    function hue2rgb_internal(p, q, t){
        if(t < 0) t += 1;
        if(t > 1) t -= 1;
        if(t < 1/6) return p + (q - p) * 6 * t;
        if(t < 1/2) return q;
        if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
        return p;
    }

    function hue2rgb(hue){
        var r,g,b;
        
        hue = hue/360;
        r = hue2rgb_internal(0.75, 0.25, hue+1/3);
        g = hue2rgb_internal(0.75, 0.25, hue);
        b = hue2rgb_internal(0.75, 0.25, hue-1/3);

        var tint = (Math.round(r * 255).toString(16)) + (Math.round(g * 255).toString(16)) + (Math.round(b * 255).toString(16));
        return tint;
    }
    
    this.addBird = function(bird){
        // if( false && !textures[bird.hue]){

        //     function hue2rgb(p, q, t){
        //         if(t < 0) t += 1;
        //         if(t > 1) t -= 1;
        //         if(t < 1/6) return p + (q - p) * 6 * t;
        //         if(t < 1/2) return q;
        //         if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
        //         return p;
        //     }

        //     var r,g,b;
        //     var hue = bird.hue/360;
        //     r = hue2rgb(0.75, 0.25, hue+1/3);
        //     g = hue2rgb(0.75, 0.25, hue);
        //     b = hue2rgb(0.75, 0.25, hue-1/3);

        //     var tint = (Math.round(r * 255).toString(16)) + (Math.round(g * 255).toString(16)) + (Math.round(b * 255).toString(16));
            

        //     // var sprite = new pixi.Sprite.fromImage('img/non-owned-bird.png');
        //     // sprite.tint = parseInt(tint, 16);


            
        //     // textures[bird.hue] = sprite.generateTexture(this.renderer, 1, pixi.SCALE_MODES.LINEAR);
            
        // }

        // console.log(textures[bird.hue]);
        // var sprite = pixi.Sprite.fromImage('img/feed.png');

        // var anotherRenderer = new pixi.CanvasRenderer(sprite.width, sprite.height, {
        //     transparent: true,
        //     antialias: true,
        //     resolution: 1
        // });
        
        // anotherRenderer.render(sprite);


        // document.body.appendChild(anotherRenderer.view);
        
        // // var texture = sprite.generateTexture(anotherRenderer);
        // var texture = pixi.Texture.fromCanvas(anotherRenderer, pixi.SCALE_MODES.DEFAULT);


        var newBirdSprite = pixi.Sprite.fromImage('img/non-owned-bird.png');
        // newBirdSprite.tint = tint;

        // console.log(tint);

        newBirdSprite.position.x = xGlobalToScene(bird.x);
        newBirdSprite.position.y = yGlobalToScene(bird.y);
        newBirdSprite.rotation = Math.atan2(bird.dY, bird.dX) - 3.66519;
        newBirdSprite.anchor.set(0.5, 0.5);



        newBirdSprite.tint = parseInt(hue2rgb(bird.hue), 16);
        this.birdSprites[bird.id] = newBirdSprite;
        this.birdContainer.addChild(newBirdSprite);
    };

    this.removeBird = function(bird){
        this.birdContainer.removeChild(this.birdSprites[bird.id]);
        delete this.birdSprites[bird.id];
    };

    this.resize = function(width, height){
        this.renderer.resize(width, height);
        this.clouds.width = width;
        this.clouds.height = height;
    };
}

exports.Scene = Scene;
