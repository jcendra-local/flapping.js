var sceneExports = require('./scene.js');
var socketExports = require('./socket.js');
var playerExports = require('./player.js');



function Game() {
    this.socket = new socketExports.Socket(this);
    this.gameStarted = false;
    this.scene = new sceneExports.Scene();
    this.player = null;
    this.animLoopHandle = null;

    this.boundingBox = {x1: 0, x2: 0, y1: 0, y2: 0};
    this.gameWidth = 0;
    this.gameHeight = 0;

    this.birds = {};

    this.start = function (playerName) {
        this.player = new playerExports.Player(playerName);
        this.socket.start();
        this.socket.createNewPlayer(playerName);
        this.socket.respawn();

        this.animLoopHandle = requestAnimFrameFunc();
        this.animLoopHandle.call(window, this.animLoop.bind(this));
        this.gameStarted = true;
    };

    function keyInput() {
    // function processKeyInput(event){
//     var key = event.which || event.keyCode;
//     if (key === 65){
//         socket.emit('3');
//     }
// }
}

    this.onRespawned = function(playerSettings, gameSize) {
        console.log("Player settings: ", playerSettings);
        this.gameWidth = gameSize.width;
        this.gameHeight = gameSize.height;
    };

    this.tellPlayerMove = function(playerData, newBirdList) {
        var deltaX = this.player.x - playerData.x;
        var deltaY = this.player.y - playerData.y;

        this.player.x = playerData.x + (playerData.w / 2);
        this.player.y = playerData.y + (playerData.w / 2);

        this.player.hue = playerData.hue;
        this.player.deltaX = isNaN(deltaX) ? 0 : deltaX;
        this.player.deltaY = isNaN(deltaY) ? 0 : deltaY;

        var newBird;

        for (var birdId in this.birds) {
            newBird = newBirdList[birdId];
            if(newBird === undefined) {
                this.scene.removeBird(this.birds[birdId]);
                delete this.birds[birdId];
                continue;
            }
            this.birds[birdId] = newBird;
        }

        for(birdId in newBirdList) {
            if(this.birds[birdId] !== undefined) continue;
            newBird = newBirdList[birdId];
            this.birds[birdId] = newBird;
            this.scene.addBird(newBird);
        }

        this.scene.boundingBoxX = playerData.x;
        this.scene.boundingBoxY = playerData.y;
        this.scene.boundingBoxWidth = playerData.w;
        this.scene.boundingBoxHeight = playerData.h;
    };

    this.animLoop = function() {
        this.animLoopHandle.call(window, this.animLoop.bind(this));
        this.tickGame();
    };

    function requestAnimFrameFunc() {
        return window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.msRequestAnimationFrame     ||
            function( callback ) {
                console.log("reverting to old time");
                window.setTimeout(callback, 1000 / 60);
            };
    }

    var lastFrameTime = new Date().getTime();
    this.tickGame = function() {
        var newFrameTime = new Date().getTime();
        document.getElementById('fps-number').innerHTML = Math.round(1000 / (newFrameTime - lastFrameTime));
        lastFrameTime = newFrameTime;
        if (!this.gameStarted) {
            this.gameStopped();
        }
        
        this.tickPlayer();
    };

    this.tickPlayer = function() {
        if (!this.gameStarted) {
            this.gameStopped();
            return;
        }

        this.scene.drawBackground();
        this.scene.drawBorders();
        this.scene.drawBirds(this.birds);

        // var action = {
        //     aX: this.scene.getMouseXCoords(),
        //     aY: this.scene.getMouseYCoords()
        // };

        // this.socket.emit('0', action);
    };

    this.stopAnimation = function () {
        // window.cancelAnimFrame = (function(handle) {
        //     return  window.cancelAnimationFrame     ||
        //             window.mozCancelAnimationFrame;
        // })();
    };

    this.resize = function(width, height) {
        console.log('Resizing screen..');
        this.scene.resize(width, height);
        this.socket.resize(width, height);
    };

    this.sendSceneSize = function() {
        console.log('Requested screen size..');
        this.socket.resize(this.scene.getSceneWidth(),
                           this.scene.getSceneHeight());
    };

    this.gameStopped = function() {
        // TODO: cleanup 
    };
}


exports.Game = Game;
