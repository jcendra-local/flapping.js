var coordinate = require('./coordinate.js');

function BoundingBox(coordinate1, coordinate2) {
    this.coordinate1 = coordinate1;
    this.coordinate2 = coordinate2;
}

exports.BoundingBox = BoundingBox;
