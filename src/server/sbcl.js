var JsonSocket = require('json-socket');
var net = require('net');
// var sock = new JsonSocket(net.Socket());


function Sbcl(file){
    var sbcl = this;
    
    this.on('connect', function(){
        this.on('message', function(message){
            // console.log(message);
            if(!message.c){
                console.log("We've received msg from SBCL with no CMD.");
                return;
            }
            if(message.d === undefined)
                console.log("We've received msg from SBCL with no DATA.'");

            sbcl._socket.emit(message.c, message.d);
        });

        console.log("Connected to SBCL, sending Hey there");

        sbcl.send('heyThere');
    });

    this.connect(file);

    this.send = function(cmd, data, done){
        this.sendMessage({c: cmd, d: data}, done);
    };
}

Sbcl.prototype = new JsonSocket(net.Socket());

module.exports = Sbcl;

