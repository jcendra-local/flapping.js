/*jslint bitwise: true, node: true */
'use strict';
console.log('Starting up Front server');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

// var performance = require('performance-now');

// Import game settings.
var c = require('../../config.json');

var Sbcl = require("./sbcl.js");
var sbcl = new Sbcl("/tmp/node-sbcl.sock");

var playerRequire = require('./player.js');

var coordinate = require('./coordinate.js');

app.use(express.static(__dirname + '/../client'));

var users = {};
var sockets = {};

var leaderboard = [];
var leaderboardChanged = false;


// Defines all the hooks
io.on('connection', function (socket) {
    console.log('A user with ID ' + socket.id + 'connected!');

    // New player's object
    var player = new playerRequire.Player(socket.id);

    socket.on('createNewPlayer', (name) => {
        console.log('[INFO] Creating player ' + name + '.');

        if (users[player.id] !== undefined) {
            console.log('[INFO] Player ID is already connected, kicking.');
            socket.disconnect();
            return;
        }

        player.name = name;
        sockets[player.id] = socket;

        sbcl.send('userConnected', {userId: player.id});
        player.hue = Math.round(Math.random() * 360);
        io.emit('playerJoined');
        socket.emit('getSceneSize');
        console.log('Total players: ' + Object.keys(users).length);

    });

    socket.on('ping', function () {
        socket.emit('pong');
    });

    socket.on('resize', function (data) {
        player.screenSize = new coordinate.Coordinate(
            data.screenWidth,
            data.screenHeight
        );

        sbcl.send("screenResized", {userId: player.id, w: data.screenWidth, h: data.screenHeight});
    });

    socket.on('respawn', function () {
        users[player.id] = player;
        socket.emit('respawned', player, {width: c.gameWidth, height: c.gameHeight});
        console.log('[INFO] User ' + player.name + ' respawned!');
    });

    socket.on('disconnect', function () {
        users[player.id] = undefined;

        console.log('[INFO] User ' + player.name + ' disconnected!');
        socket.broadcast.emit('playerDisconnect', { name: player.name });
    });

    // Heartbeat function, update everytime.
    socket.on('0', function(target) {
        player.lastHeartbeat = new Date().getTime();
        if (target.x !== player.x || target.y !== player.y) {
            // currentPlayer.target = target;
            // currentPlayer.aX = target.aX;
            // currentPlayer.aY = target.aY;
        }

    });

    socket.on('3', function(){
        sbcl.send('addBirdToUser', {userId: player.id});
        // var newBird = new birds.Bird({x: currentPlayer.x, y: currentPlayer.y}, currentPlayer.id);
        // currentPlayer.birds[newBird.id] = newBird;
    });
});


function requestUpdates(){
    Object.keys(users).forEach(function(userId){
        sbcl.send('requestUserView', {userId: userId});
    });
}

sbcl.on("userView", sendUserUpdate);

function sendUserUpdate(data){
    var playerData = {
        x: data.x,
        y: data.y,
        w: data.w,
        h: data.h,
        hue: data.hue
    };

    sockets[data.userId].emit('serverTellPlayerMove', playerData, data.birds);
}

setInterval(requestUpdates, 1000 / c.networkUpdateFactor);

// Don't touch, IP configurations.
var ipaddress = process.env.OPENSHIFT_NODEJS_IP || process.env.IP || '127.0.0.1';
var serverport = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || c.port;
if (process.env.OPENSHIFT_NODEJS_IP !== undefined) {
    http.listen( serverport, ipaddress, function() {
        console.log('[DEBUG] Listening on *:' + serverport);
    });
} else {
    http.listen( serverport, function() {
        console.log('[DEBUG] Listening on *:' + c.port);
    });
}
