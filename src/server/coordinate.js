function Coordinate(x, y) {
    this.x = x;
    this.y = y;

    this.getX = function() {
        return this.x;
    };

    this.getY = function() {
        return this.y;
    };

    this.setX = function(x) {
        this.x = x;
    };

    this.setY = function(y) {
        this.y = y;
    };
}

exports.Coordinate = Coordinate;













