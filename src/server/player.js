var boundingBox = require('./boundingBox.js');
var coordinate = require('./coordinate.js');

function Player(id) {
    this.id = id;
    this.name = 'Unknown player';
    this.lastHeartbeat = new Date().getTime();
    this.hue = 0;
    this.target = {
        x: null,
        y: null
    };

    this.x = 400;
    this.y = 400;
    this.target.x = 0;
    this.target.y = 0;
    this.birds = {};
    this.boundingBox = new boundingBox.BoundingBox(
        new coordinate.Coordinate(this.x - 30, this.y - 30),
        new coordinate.Coordinate(this.x + 30, this.y + 30)
    );

    this.screenSize = new coordinate.Coordinate(1, 1);
}

exports.Player = Player;
