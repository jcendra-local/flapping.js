(in-package :flapping)

*game-tick-timer*

(start-timer)
(stop-timer)

*game-tick-timer*

*users*


(push-queue (list :add-bird-to-user (id (car *users*)))
            *game-queue*)


(stop-timer)


(game-side  *grid*)

(angle-diff 0 0 0 0)

(do ((val 0 (1+ val)))
    ((>= val 10))
  (if (=  val 1)
      (continue))
    (print val))

(defparameter *print-once* t)

;; set random positions
(dll-iterate (bird (owned-birds  (car *users*)))
  (set-new-pos bird (random 1000) (random 1000)))

(let ((num-birds 0))
  (dll-iterate (bird (owned-birds (car *users*)))
    (incf num-birds))
  num-birds)

;; set random speed
(dll-iterate (bird (owned-birds (car *users*)))
  (set-new-speed bird (+ *bird-min-speed* (random (- *bird-max-speed* *bird-min-speed*)))))


(defun set-new-speed (bird new-speed)
  (let ((speed (pythagoras (dx bird) (dy bird))))
    (when (> 0 speed)
      (setf (dx bird) (* new-speed (/ (dx bird) speed)))
      (setf (dy bird) (* new-speed (/ (dy bird) speed))))))

(defun set-new-pos (bird x y)
  (setf (x bird) x)
  (setf (y bird) y))




(destructuring-bind (one two) (list 1 2)
  (print one)
  (print two))



(+ 11/10
   (/ 1 (- (* 10/11 0))))

(x - 1.1) / 11

1.1 + 
11/100 * (x - 1.1)



100/11x - 110/11


1.1 + (1 / ((10/11 * 10 * x) - 10))

(loop for i from 0 to 1 by 0.01 collect
     (= (+ 1.1 (* 11/100 (- i 1.1)))
        (+ 1.1
           (/ 1 
              (- (* 10/11 10 i) 10)))))
(every #'identity
       (loop for i from 0 to 1 by 1/100 collect 
            (= (print  (+ 11/10
                          (/ 1 (* 100/11 (- i 11/10)))))

               (print  (+ 11/10
                          (/ 1
                             (* 10 (- (* 10/11 i) 1))))))))


(stop-server)
(stop-timer)


*game-tick-timer*
(setf  *print-once* t)
